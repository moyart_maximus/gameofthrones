(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'home': '/',
      'house': '/house',
      'detail': '/detail'
    }
  });

}(document));
