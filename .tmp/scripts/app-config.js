/* eslint-disable */(function(window) {
  'use strict';
  window.AppConfig = {
    'active': true,
    'target': '.tmp',
    'deployEndpoint': '',
    'i18nPath': 'locales/',
    'componentsPath': './components/',
    'composerEndpoint': './composerMocks/',
    'endpoint': 'https://anapioficeandfire.com/api/',
    'appId': '',
    'debug': true,
    'mocks': true,
    'coreCache': true,
    'routerLog': false,
    'cordovaScript': 'cordova.js',
    'prplLevel': 1,
    'initialBundle': [
      'home.json'
    ],
    'transpile': true,
    'transpileExclude': [
      'cells-rxjs',
      'custom-elements-es5-adapter',
      'moment',
      'd3',
      'bgadp*'
    ]
  };
}(window));